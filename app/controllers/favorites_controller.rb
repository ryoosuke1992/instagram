class FavoritesController < ApplicationController
  before_action :authenticate_user!
  def create
    @post = Post.find(params[:id])
    @favorite = Favorite.new(post_id: @post.id, user_id: current_user.id)
      if @favorite.save
        @post.create_notification_favorite!(current_user)
        flash[:success] = "お気に入りに登録しました"
        redirect_to :back
      else
        flash[:danger] = "お気に入りに登録できませんでした。(すでに登録済かも？)"
        redirect_to :back
      end
  end
end
