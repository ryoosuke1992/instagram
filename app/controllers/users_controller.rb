class UsersController < ApplicationController
  
  before_action :authenticate_user!
  
  def index
    @users = User.all.order("created_at DESC").page(params[:page]).per(10)
  end
  
  def show
    @user = User.find(params[:id])
    @posts = Post.where(user_id: params[:id])
  end
  
  def destroy
  end
  
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def search
    @users = User.page(params[:page]).per(10).search(params[:search])
  end
end
