class CommentsController < ApplicationController
  before_action :authenticate_user!
    
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    @comment.user_id = current_user.id if user_signed_in?
    @posts = @comment.post
      if @comment.save
        @posts.save_notification_comment!(current_user, @comment.id, @post.user.id)
        flash[:success] = "コメントしました"
        redirect_to :back
      else
        flash[:danger] = "コメントに失敗しました"
        redirect_to :back
      end
  end

  private
  
    def comment_params
      params.require(:comment).permit(:content)
    end
end