class PostsController < ApplicationController
  
  before_action :authenticate_user!
  
  def index
    @posts = Post.where(user_id: current_user.following).order("created_at DESC").page(params[:page]).per(10)
    @comment = Comment.new
  end
  
  def create
    @post = Post.create(post_params)
    @post.user_id = current_user.id if user_signed_in?
    if @post.save
      redirect_to post_path @post
      flash[:notice] = "投稿に成功しました"
    else
      render :new
      flash[:danger] = "投稿に失敗しました"
    end
  end
    
  def new
    @post = Post.new
  end
    
  def show
    @post = Post.find(params[:id])
    @user = User.find_by(id: @post.user_id)
    @comment  =  @post.comments.build
    @comments =  Comment.where(post_id: @post.id).order("created_at DESC")
  end
    
  private
      
    def post_params
      params.require(:post).permit(:image, :description)
    end
end
