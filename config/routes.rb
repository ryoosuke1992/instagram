Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "home#toppage"
  get "/search", :to => "users#search" 
  resources :rules, only: [:index]
  devise_for :users, :controllers => {
    :registrations => 'users/registrations',
    :sessions => 'users/sessions',
    :omniauth_callbacks => 'users/omniauth_callbacks'
  } 
  devise_scope :user do
    get "users/sign_in", :to => "users/sessions#new"
    get "/users/sign_out", :to => "users/sessions#destroy" 
    get 'password_edit' => 'users/registrations#password_edit'
  end
  
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :posts do 
    member do 
      post "add", to: "favorites#create"
    end
    resources :comments
  end
  
  resources :favorites, only: [:destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :notifications, only: :index
end
